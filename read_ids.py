# -*- coding: utf-8 -*-

################################################################################
#
#      Connect to Quandal
#
################################################################################

from QuandlUtils import *
import pandas as pd
import collections

import fileUtils as fu

################################################################################
#
#      Helper Functions
#
################################################################################

def cleanName(rawName):
    """
    Cleans the raw name returned from Quandl WIKI database
    :param rawName: a string with raw name, <company name> Prices ....
    :return: String, just the company name
    """
    # drop everything after Prices

    # find index of 'Prices,...' this marks the beg of description
    indexOfPrices = rawName.rfind('Prices,')

    if(indexOfPrices>0):
        # take everything from beg of string up to 'Prices,....'
        # use sequence nature of String type
        cleanName = rawName[:indexOfPrices]
    else:
        cleanName = rawName

    # trim any white space from beg and end of cleanName for final result
    return cleanName.strip()


def getPageMetaData(pageNo, authtoken=QUANDLTOKEN, verbose=False):
    """
    Returns a page of meta data results for WIKI, None if done
    :param pageNo:
    :param verbose:
    :return: List, or None if no results at pageNo
    """
    pageResult = Quandl.search("*", source="WIKI", page=pageNo, authtoken=authtoken, verbose=verbose)
    # if results then return the page list of results, otherwise False
    if (pageResult):
        return pageResult
    else:
        return None

def processPageMetaData(rawPage, verbose=False):
    """
    returns a list structured for appending to master meta data list
        rawpage is list of dictionaries
        this function creates a list of lists
        where each row (inner list) contains
            <id>, <name>, <rawPage.dictionary>
    :param rawPage:
    :return: a list for appending to master list
    """

    # pluck all the 'code' ids from the dictionaries in rawPage list
    ids = map(lambda row: row['code'], rawPage)

    # pluck all the 'company names from the dictionaries in rawPage list
    names = map(lambda row: cleanName(row['name']), rawPage)

    # zip ids, names and rawpage rows together
    # map list over result because zip returns a list of tuples
    # wrap map() in list because py 3.x map returns a map not a list
    # cleanPage is a list of lists
    cleanPage = list(map(list, zip(ids, names, rawPage)))

    if (verbose):
        print("=============================================================================")
        for row in cleanPage:
            print(row[0], " : ", row[1])

    return cleanPage

def getMetaData(authtoken=QUANDLTOKEN, maxPages=None, verbose=False):
    """
    Returns a master list of the meta data for the WIKI database
    :param authtoken:
    :param maxPages: the maximum number of pages to read, if None then read all
    :param verbose:
    :return: list of lists, <id>, <coname>, <metadata>
    """

    metaDataMasterList = []
    pageNo = 1
    # getPageMetaData(pageNo=pageNo, verbose=verbose) does all the work fetching data
    # note wrapping in lambda, idiom req by py wrapping functions with args
    for page in iter(lambda: getPageMetaData(pageNo=pageNo, verbose=verbose), None):
        if ((pageNo % 5) == 0):
            print("Processing page {}".format(pageNo))
        # process the page putting in form can be added to master list
        processedPage = processPageMetaData(rawPage=page, verbose=verbose)
        metaDataMasterList += processedPage
        pageNo += 1
        # exit if you have read maxPages
        if (maxPages and (pageNo > maxPages)): break

    print("="*90)
    print("Done processing")
    print("Total number of firms processed = {:d}".format(len(metaDataMasterList)))

    return metaDataMasterList



def rawDump(rawPage):
    print("=============================================================================")
    for item in rawPage:
        print(item['code'], " : ", item['name'])



def main():
    #mydata2 = Quandl.search("*", source="WIKI", page=1, authtoken=QUANDLTOKEN, verbose=True)

    # mydata2 = getPageMetaData(1)
    # processPageMetaData(mydata2, verbose=True)
    # rawDump(mydata2)

    masterList = getMetaData(maxPages=None, verbose=False)

    idList = [row[0] for row in masterList]
    nameList = [row[1] for row in masterList]
    metaDataList = [row[2] for row in masterList]

    # create a pd DataFrame from list of dictionaries, index by id
    metaDataDF = pd.DataFrame(metaDataList, index=idList)
    # create a pd DataFrame of just conames
    nameDF = pd.DataFrame({'coname': pd.Series(nameList, index=idList)})

    print(metaDataDF)
    print(nameDF)

    #####################################################################
    # store data in hfd5 files

    QUANDLWIKIfilename = 'quandl.wiki.h5'

    # set working dir to whatever dir this file is in
    fu.setCWDToFileDirectory(__file__)
    # open the file, get a handle
    h5Store = pd.HDFStore(QUANDLWIKIfilename)
    # store conames with ids and company names
    h5Store['conames'] = nameDF
    h5Store.flush()
    # store the meta data on names (shouldn't really ever need this)
    h5Store['metaData'] = metaDataDF
    h5Store.flush()

    h5Store.close()



if __name__ == "__main__":
    main()

