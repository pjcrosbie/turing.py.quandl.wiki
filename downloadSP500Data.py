# -*- coding: utf-8 -*-

################################################################################
#
#   Download historical S&P 500 data
#   Primary interest in this data is the dates to get a good set of market open
#   dates to reindex stock price data, reindexing on a global set of open days
#   ensures you have identified missing days correctly
#
#       Quandl.get("YAHOO/INDEX_GSPC")
#       https://www.quandl.com/data/YAHOO/INDEX_GSPC-S-P-500-Index
################################################################################

from QuandlUtils import *
import pandas as pd
import numpy as np
from math import log

# this allows you to reload modules in live interpreter
from importlib import reload

# this allows multiple function overloading (signature determines function dispatch)
from multipledispatch import dispatch

# my utils, see _myLib\myLib3
import fileUtils as fu
import Timer
from utils import *

# get QUANDLTOKEN
from QuandlUtils import *


QUANDLSP500filename = 'quandl.yfinance.h5'
# set working dir to whatever dir this file is in
fu.setCWDToFileDirectory(__file__)

################################################################################
#
#      Helper Functions
#
################################################################################

def toDateString(aDatetime64):
    """
    Returns string form of the date in aDatetime64, a datetime64 object
    :param aDatetime64:
    :return: String
    """
    # converts to string form and returns just the first 10 chars (dropping time)
    # yyyy-mm-dd , 10 chars!
    dateStr = str(aDatetime64)[:10]
    return dateStr

dispatch(pd.DataFrame)
def getMostRecentDate(aTimeSeriesDF):
    """
    Returns the most recent date from a datetime64 indexed DataFrame
    :param aTimeSeriesDF: DataFrame
    :return: datetime64
    """
    # get the index from the DF, assumes that it is dates
    dates = aTimeSeriesDF.index.values
    # sorts them, probably not necessary but prob very cheap anyway
    dates.sort()
    # take the last item
    return dates[-1]

dispatch(np.array)
def getMostRecentDate(dateArray):
    """
    Returns the most recent date from a datetime64 indexed DataFrame
    :param dateArray: numpy.array of dates
    :return: datetime64
    """
    dates = dateArray.copy()
    # sorts them, probably not necessary but prob very cheap anyway
    dates.sort()
    # take the last item
    return dates[-1]

#################################################################################################
#
#   key functions

def getSP500Series(startDateStr=None, endDateStr=None, authtoken=QUANDLTOKEN, verbose=False):
    """
    Gets EOD index level data from Quandl/YAHOO db
    :param startDateStr: String, start date 'yyyy-mm-dd', optional None, return all to endDateStr
    :param endDateStr: String, end date 'yyyy-mm-dd', optional None, return all from startDateStr to end
    :param authtoken: defaults to QUANDLTOKEN
    :param verbose:
    :return: pd DataFrame
    """
    rawDF = Quandl.get("YAHOO/INDEX_GSPC", trim_start=startDateStr, trim_end=endDateStr )

    return rawDF

def storeSP500Data(filename, sp500DF):
    """
    stores the sp500DF into hdf5 file filename using valid python name name
    :param filename: String, hdf5 filename
    :param sp500DF: DataFrame
    :return: None
    """

    # open the file, get a handle
    h5 = pd.HDFStore(filename)

    # store the raw sp500DF
    h5['rawPriceData'] = sp500DF

    # dates, rounded to just days
    # DataFrame uses dtype='datetime64[ns]'
    h5['dates'] = pd.DataFrame({
        # a simple date day datetime64 object
        'date' : np.array(sp500DF.index.values, dtype='datetime64[D]'),
        # a simple string representation of the date, 'yyyy-mm-dd'
        'dateStr' : [str(aDate) for aDate in np.array(sp500DF.index.values, dtype='datetime64[D]').tolist()],
        },
        index=sp500DF.index.values
    )

    # log returns
    # Adjusted Close provides the closing price for the requested day adjusted
    # for all applicable splits and dividend distributions. Data is adjusted
    # using appropriate split and dividend multipliers, adhering to
    # Center for Research in Security Prices (CRSP) standards.
    # see: https://help.yahoo.com/kb/finance/historical-prices-sln2311.html

    prices = sp500DF['Adjusted Close']

    # calc logRets using adjust to pct_change()
    logRets = prices.pct_change().apply(lambda pctChange: log(pctChange + 1))

    h5['logRets'] = logRets

    h5.flush()
    h5.close()

def main():

    # downloads everything from QUANDL/Yahoo SP500 index
    # "YAHOO/INDEX_GSPC"
    sp500DF = getSP500Series()

    # set working directory to the same directory as this file
    fu.setCWDToFileDirectory(__file__)

    # saves the data to a HDF5 file
    filename = 'sp500Data(2015.05.16).h5'
    storeSP500Data(filename, sp500DF)

    h5 = pd.HDFStore(filename, 'r')
    print(h5)

if __name__ == "__main__":
    main()
else:
    # this prints the name of the sourcefile executing
    # see: http://stackoverflow.com/questions/2632199/how-do-i-get-the-path-of-the-current-executed-file-in-python
    from inspect import getsourcefile
    from os.path import abspath
    print('loaded py file: {}'.format(abspath(getsourcefile(lambda:0))))


