# -*- coding: utf-8 -*-

################################################################################
#
#      Download EOD Price Data from Quandl WIKI db
#      https://www.quandl.com/data/WIKI
#
################################################################################

from QuandlUtils import *
import pandas as pd

import fileUtils as fu
from pprint import pprint as p
import Timer

QUANDLWIKIfilename = 'quandl.wiki.tmp.h5'
# set working dir to whatever dir this file is in
fu.setCWDToFileDirectory(__file__)

################################################################################
#
#      Helper Functions
#
################################################################################

def printDoubleLine():
    print("="*80)


def getPriceSeries(id, authtoken=QUANDLTOKEN, verbose=False):
    """
    Gets EOD price series data from Quandl/WIKI EOD price db
    :param id: (e.g. WIKI/AAPL)
    :param authtoken: defaults to QUANDLTOKEN
    :param verbose:
    :return: pd DataFrame
    """

    rawDF = Quandl.get(id, authtoken=authtoken)


    return rawDF


def takeTicker(id):
    """
    Returns just the ticker from the id
    WIKI/AAPL -> AAPL
    :param id:
    :return: string
    """
    parts = id.split('/')
    return parts[1]

def reindexPriceDF(id, rawPriceDF):
    """
    Returns a new priceDF with a multi index, <ticker> <date>
    :param rawPriceDF: DataFrame indexed by <date>
    :return: DataFrame indexed by <ticker> <date>
    """
    ticker = takeTicker(id)
    # raw priceDF is indexed by date only, no ticker
    dates = rawPriceDF.index.values
    # create new multiIndex ticker date
    multiIndex = pd.MultiIndex.from_product([ticker, dates], names=['ticker', 'date'])
    priceDF = rawPriceDF.set_index(multiIndex)

    return priceDF


def storeData(name, aDF):
    #####################################################################
    # store data in hfd5 files


    # open the file, get a handle
    h5Store = pd.HDFStore(QUANDLWIKIfilename)
    # store conames with ids and company names
    h5Store[name] = aDF
    h5Store.flush()

    h5Store.close()

def getData(name):
    h5Store = pd.HDFStore(QUANDLWIKIfilename)
    print(h5Store)
    aDF = h5Store.get(name)
    h5Store.close()

    return(aDF)

def main():
    #mydata2 = Quandl.search("*", source="WIKI", page=1, authtoken=QUANDLTOKEN, verbose=True)

    # mydata2 = getPageMetaData(1)
    # processPageMetaData(mydata2, verbose=True)
    # rawDump(mydata2)
    # get list of ids, list of 'WIKI/AAPL', ...
    QUANDLWIKIfilename = 'quandl.wiki.h5'
    printDoubleLine()
    print("Open file {}".format(QUANDLWIKIfilename))
    h5store = pd.HDFStore(QUANDLWIKIfilename)
    print("Contents:", h5store)
    # read the contents of conames
    conames = h5store.get('conames')
    h5store.close()
    # idList is the list of ids 'WIKI/AAPL', ...
    idList = conames.index.values

    # idList = idList[:1]   #TODO just temp for debugging

    # priceDataList is a list of DataFrames, indexed by ticker and date
    printDoubleLine()
    print('Downloading {:,d} firms\' price data from Quandal'.format(len(idList)))
    priceDataList = []
    idCount = 1
    with Timer.Timer():
        # note potential memory issue here
        # Timer() class temporarily turns off garbage collection
        for id in idList:
            # download the price time series from Quandl
            rawPriceDF = getPriceSeries(id, authtoken=QUANDLTOKEN, verbose=False)
            # set index to id date instead of just date
            priceDF = reindexPriceDF(id, rawPriceDF)
            # append priceDF to list of DFs
            priceDataList.append(priceDF)
            if ((idCount % 5) == 0):
                print('Processed firm: {:,d}'.format(idCount))
            idCount += 1

    print('Downloaded {:,d}'.format(len(priceDataList)))

    # concat priceDataList to priceDataset
    priceDataset = pd.concat(priceDataList)

    # save priceDataset to file
    QUANDLWIKIDatasetFilename = 'quandl.wiki.dataset.h5'
    printDoubleLine()
    print("Open file {}".format(QUANDLWIKIDatasetFilename))
    h5store = pd.HDFStore(QUANDLWIKIDatasetFilename)
    h5store['quandl_wiki_2015_05_09'] = priceDataset
    print("Contents:", h5store)
    h5store.close()
    printDoubleLine()


def oldTest():

    id = 'WIKI/AAPL'
    # aDF = getPriceSeries(id)
    # storeData(id, aDF)

    tDF = getData(id)

    print(tDF)

    t2DF = reindexPriceDF(id, tDF)

    p(t2DF)




if __name__ == "__main__":
    main()

