# Quandl Wiki Dataset #

The Quandl Wiki dataset is an opensource EOD *cleaned* price series
The series are split and dividend adjusted.
There are about 3,000 US equities in Wiki

There are of course no guarantees as to the data quality. For example, I note that the coname data is not complete. 
There are some tickers without names. 

This module is mostly a working prototype for Quandl data and of course python 3.x

* Queries Wiki to get ticker list
* Saves ticker list to HDF5 file

* Loops through ticker list downloading data and building dataset
* Saves dataset to HDF5 file
* The data is stored in *hdf5* file `quandl.wiki.dataset.h5`, in the pandas DataFrame `quandl_wiki_2015_05_09`
* The process takes almost 5 hours to download the twenty year history for approx 3,000 firms.

## Download Tickers ##

The file `read_ids.py` downloads the meta data for the WIKI db. It stores the data in the *hdf5* file `quandl.wiki.h5`
The download process is pretty quick, takes perhaps 10 mins.


## Download Price Data ##

The file `downloadPriceData.py` downloads the price data for the tickers in `quandl.wiki.h5`. It downloads the 
price history one ticker at a time to be polite to the Quandl site. I have an unlimited download key but they requested
I avoid using parallel downloading.

The `Quandl.py` provided library `Quandl.get()` returns the downloaded data in a pandas DataFrame. The raw DataFrame is
indexed only by date. We reset the index of the DataFrame (which creates a new DataFrame) to ticker and date using a
pandas MultiIndex. We store these reindexed DataFrames in a simple list and repeat the process downloading the price
history for each ticker. This is obviously the bulk of the time, about 5-10 seconds per firm. Total time is about
almost 5 hours to download the twenty year history for approx 3,000 firms.

Once we have completed the downloading process we have all the price history for all the firms in a large list, one 
DataFrame per firm. We use the pandas concat function to combine all the DataFrames into one large DataFrame.

The final single large DataFrame containing all of the price history data indexed by ticker and date is stored in the
*hdf5* file `quandl.wiki.dataset.h5` in the pandas DataFrame pandas DataFrame `quandl_wiki_2015_05_09`. The *hdf5* file
is about 1.4 GB (350 MB zipped).

## Accessing the Price Data in *HDF5* File ##
  
The file `readHDF5_price.py` illustrates how to access the price history in the `quandl.wiki.dataset.h5` 

## TODO ##

* Add compression filter to *hdf5* file to automatically compress data on disk and avoid the zip step. The compression 
ratio is pretty good, about 1/3 of uncompressed.
* Tidy up file names
* Tidy up code
* Add this to some production process somewhere to grab the data on a daily or weekly basis and update the datafile
* Test the relative performance of *sqlite* and *hdf5* (and other candidate databases such as *Mongodb* and *OrientDB*) 
although I think we already know the performance answer. However, it should be noted the *hdf5* drivers do not support
multi-user access so there is a architectural design issue about using *hdf5* in a production context.
 



