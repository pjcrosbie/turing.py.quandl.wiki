# -*- coding: utf-8 -*-

################################################################################
#
#       Quandal Utils
#
#       NB this has pjc token!
#
################################################################################

import Quandl.Quandl as Quandl

# this is my authorization token from the Quandl site, see profile page of my account
# should be unlimited calls per day but Quandl requests do not download in parallel
QUANDLTOKEN = "tRmTZYsxtw47MQyLdW9t"


def pingQuandl(quandlToken = QUANDLTOKEN):
    """
    Tests connection to Quandl server
    :param quandlToken: optional, defaults to module QUANDLTOKEN
    :return: True if connection successful, False otherwise
    """

    ping = False

    try:
        # gets unemployment rates for USA, should use any fast query
        result = Quandl.get("ODA/USA_LUR", authtoken=QUANDLTOKEN)
        ping = True
    except: # catch *all* exceptions
        ping = False

    return ping

