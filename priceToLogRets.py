# -*- coding: utf-8 -*-

################################################################################
#
#       Process raw prices to log returns
#       Inputs:
#           DataFrame of raw prices, indexed by ticker and date
#
#       Outputs:
#           DataFrame of logReturns, indexed by ticker and date
#
#       Reindexes all stocks to have same dates
################################################################################

from QuandlUtils import *
import pandas as pd
import numpy as np
from pprint import pprint as pl

# my utils, see _myLib\myLib3
import fileUtils as fu
import Timer

import readHDF5_price
from readHDF5_price import printDoubleLine

#################################################################################
#
#       Helper functions

def printBasicInfo(priceDF):

    #print the names of the multiindex
    print(priceDF.index.names)
    #   get the components of the multilevel index
    tickers = priceDF.index.get_level_values('ticker').unique()
    dates = priceDF.index.get_level_values('date').unique()

    # print the results
    print('Index ticker type {} and count unique index values {:,d}'.format(type(tickers), len(tickers)))
    # there is something wrong with the dates because this shows 13,000 unique dates, must be some time issues for EOD
    print('Index dates type {} and count unique index values {:,d} (approx years {:,f})'.format(type(dates), len(dates), len(dates)/250))

    printDoubleLine()
    print('Ticker : nobs : years')
    for ticker in tickers.tolist():
        prices = priceDF.loc[ticker]
        print('{} : {} : {}'.format(ticker, len(prices), len(prices)/250))











def main():

    # reads price history from a hdf5 file in same dir
    priceHistoryDF = readHDF5_price.readPriceData()

    printBasicInfo(priceHistoryDF)


if __name__ == "__main__":
    main()

