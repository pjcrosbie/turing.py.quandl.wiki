# -*- coding: utf-8 -*-

################################################################################
#
#      Read Quandal ids file
#
################################################################################

from QuandlUtils import *
import pandas as pd


import fileUtils as fu

# set working dir to whatever dir this file is in
fu.setCWDToFileDirectory(__file__)

QUANDLWIKIfilename = 'quandl.wiki.h5'

def printDoubleLine():
    print("="*80)


printDoubleLine()
print("Open file {}".format(QUANDLWIKIfilename))
h5store = pd.HDFStore(QUANDLWIKIfilename)
print("Contents:")
print(h5store)

# read the contents of conames
conames = h5store.get('conames')
print('Length of conames: {:,d}'.format(len(conames)))

# close the file
h5store.close()

# print the contents
maxLines = 13
currLineNo = 1
printDoubleLine()
print('loop through conames, maxLines: {:,d}'.format(maxLines))
# iterrows returns a tuble with idx and row contents
for idx, row in conames.iterrows():
    # break out of loop if more than maxLines printed
    if (currLineNo > maxLines): break
    print("{} \t {}".format(idx, str(row['coname'])))
    currLineNo += 1

