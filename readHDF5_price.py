# -*- coding: utf-8 -*-

################################################################################
#
#       Read Quandal price file
#       This file illustrates accessing the price history data in the hdf5 file
#
################################################################################

from QuandlUtils import *
import pandas as pd
import numpy as np
from pprint import pprint as pl

# my utils, see _myLib\myLib3
import fileUtils as fu
import Timer

################################################################################
#
#      Helper Functions
#
################################################################################

def printDoubleLine():
    print("="*80)


def readPriceData():
    # set working dir to whatever dir this file is in
    fu.setCWDToFileDirectory(__file__)

    # this is just a constant with the price history dataset hdf5 filename
    QUANDLWIKIDatasetFilename = 'quandl.wiki.dataset.h5'

    # open the file, print contents summary
    printDoubleLine()
    with Timer.Timer():
        print("Open file {}".format(QUANDLWIKIDatasetFilename))
        h5store = pd.HDFStore(QUANDLWIKIDatasetFilename)
        print("Contents:", h5store)
        # read all the price history into memory, a pandas DataFrame
        print('Read the price history DataFrame into memory...')
        priceHistoryDF = h5store.get('quandl_wiki_2015_05_09')
        print('Length of priceHistoryDF : {:,d}'.format(len(priceHistoryDF)))
        # close the file
        h5store.close()

    printDoubleLine()
    return priceHistoryDF


################################################################################
#
#      Main
#
################################################################################

def main():

    priceHistoryDF = readPriceData()

    # with Timer.Timer():
    #     print('Get the index and convert numpy 2D array ...')
    #     # get the index
    #     index = priceHistoryDF.index.values
    #     # index is a numpy array of tuples, note shape
    #     print('The type of the index is {}'.format(type(index)))
    #     print('The shape of the index is {}'.format(index.shape))
    #
    #     # this converts index to a n x 2 matrix, a bit slow because first change to list of tuples
    #     index2 = np.asarray(index.tolist())
    #     # index is a numpy array of tuples, note shape
    #     print('The type of the index is {}'.format(type(index2)))
    #     print('The shape of the index is {}'.format(index2.shape))
    #
    #     # tickers are in first column of index
    #     ticker = index2[:,0]
    #     # dates are in the second column of index
    #     dates = index2[:,1]

    printDoubleLine()

    with Timer.Timer():
        print('Get the index parts directly ...')

        # print('Index names {}'.format(priceHistoryDF.index))
        #print(priceHistoryDF)
        print(priceHistoryDF.index.names)
        tickers = priceHistoryDF.index.get_level_values('ticker').unique()
        dates = priceHistoryDF.index.get_level_values('date').unique()
        # this rounds the datetimes to datetimes with no time, just the day portion
        cleanDates = np.array(dates, dtype='datetime64[D]')
        #this sorts the dates
        cleanDates.sort()
        # print the results
        print('Index ticker type {} and count unique index values {:,d}'.format(type(tickers), len(tickers)))
        # there is something wrong with the dates because this shows 13,000 unique dates, must be some time issues for EOD
        print('Index dates type {} and count unique index values {:,d} (approx years {:,f})'.format(type(dates), len(dates), len(dates)/250))
        # here are the cleaned dates, round to Day to get rid of the time portion which should only be noise
        print('Index dates type {} and count unique index values {:,d} (approx years {:,f})'.format(type(cleanDates), len(cleanDates), len(cleanDates)/250))

        printDoubleLine()
        print('Ticker : nobs : years')
        for ticker in tickers.tolist():
            prices = priceHistoryDF.loc[ticker]
            print('{} : {} : {}'.format(ticker, len(prices), len(prices)/250))


def tmp():
    # print the contents
    maxLines = 13
    currLineNo = 1
    printDoubleLine()
    print('loop through conames, maxLines: {:,d}'.format(maxLines))
    # iterrows returns a tuble with idx and row contents
    for idx, row in conames.iterrows():
        # break out of loop if more than maxLines printed
        if (currLineNo > maxLines): break
        print("{} \t {}".format(idx, str(row['coname'])))
        currLineNo += 1


    h5store.close()
    printDoubleLine()


if __name__ == "__main__":
    main()

